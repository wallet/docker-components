#!/bin/bash

# Validate mandatory runtime environment variables

if [ -z $JENKINS_URL ];
then
    echo "Missing JENKINS_URL environment variable"
    exit 1
fi

if [ -z $JENKINS_JNLP_SECRET ];
then
    echo "Missing JENKINS_JNLP_SECRET environment variable"
    exit 2
fi

if [ -z $JENKINS_NODE_NAME ];
then
    echo "Missing JENKINS_NODE_NAME environment variable"
    exit 3
fi

# Update Jenkins agent when configured or fetch it when missing

if [ "$UPDATE_AGENT" == "true" ] || [ ! -f agent.jar ];
then
  curl $JENKINS_URL/jnlpJars/agent.jar -o agent.jar
fi

if [ ! -f agent.jar ];
then
    echo "Missing agent.jar, not part of image or not downloaded"
    exit 4
fi

echo "Starting agent for node $JENKINS_NODE_NAME on host=$JENKINS_URL, workDir=$JENKINS_WORK_DIR"

java -Dhudson.slaves.ChannelPinger.pingInterval=2 -jar agent.jar -jnlpUrl $JENKINS_URL/computer/$JENKINS_NODE_NAME/slave-agent.jnlp -secret $JENKINS_JNLP_SECRET -workDir $JENKINS_WORK_DIR
