# Jenkins master

## Build docker image

Build argument `--no-cache` is required to always download plugins so that they are updated.

#### Production / mocked udgetbakers.azurecr.io/

```bash
docker pull jenkins/jenkins:lts && \
docker build -t budgetbakers.azurecr.io/jenkins-docker:azure --no-cache . && \
docker push budgetbakers.azurecr.io/jenkins-docker:azure
```

#### Minikube / local build

```bash
eval $(minikube docker-env) && \
docker pull jenkins/jenkins:lts && \
docker build -t budgetbakers.azurecr.io/jenkins-docker:azure --no-cache .
```
