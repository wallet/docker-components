#!/bin/bash

jenkins_uid=$1
jenkins_guid=$2
jenkins_username=jenkins

if [ -z "${jenkins_uid}" ]; then
  echo "'jenkins_uid' not set - do nothing.";
  exit 0;
fi;

if [ -z "${jenkins_guid}" ]; then
  jenkins_guid=$jenkins_uid;
fi;

current_guid=$(id -g ${jenkins_username})
current_uid=$(id -u ${jenkins_username})
current_uid_guid="${current_uid}:${current_guid}"
required_uid_guid="${jenkins_uid}:${jenkins_guid}";

echo "Required uid:guid=${required_uid_guid}, current jenkins uid:guid=${current_uid_guid}";

if [ "${required_uid_guid}" != "${current_uid_guid}" ]; then
      echo "not the same  -  will change";
      usermod -u $jenkins_uid $jenkins_username || exit 1
      find / \( -path /proc -or -path /dev \) -prune -o -user ${current_uid} -exec chown -h ${jenkins_uid} {} \; || exit 4
      echo "${jenkins_username} uid changed to ${jenkins_uid}"
      groupmod -g $jenkins_guid $jenkins_username || exit 2
      find / \( -path /proc -or -path /dev \) -prune -o -group ${current_guid} -exec chgrp -h ${jenkins_guid} {} \; || exit 3
      echo "${jenkins_username} guid changed to ${jenkins_guid}"
else
  echo "Required uid:guid is the same, do nothing.";
fi;

