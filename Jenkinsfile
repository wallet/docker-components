registryCredentialsId = 'bbregistry'
registryUrl = "http://budgetbakers.azurecr.io"

properties([[
                $class  : 'BuildDiscarderProperty',
                strategy: [
                    $class               : 'LogRotator',
                    artifactDaysToKeepStr: '',
                    artifactNumToKeepStr : '',
                    daysToKeepStr        : '',
                    numToKeepStr         : '10'
                ]],
            parameters([
                booleanParam(description: 'use --no-cache argument when building Docker image', name: 'noCache', defaultValue: true),
                booleanParam(description: 'Ubuntu base image Xenial 16.04 LTS', name: 'ubuntuXenial'),
                booleanParam(description: 'Ubuntu base image Bionic 18.04 LTS', name: 'ubuntuBionic'),
                booleanParam(description: 'Java 13 base image on Ubuntu 18.04', name: 'java13Bionic'),
                booleanParam(description: 'SBT base image', name: 'sbt'),
                booleanParam(description: 'Jenkins master', name: 'jenkinsMaster'),
                booleanParam(description: 'Jenkins slave common image', name: 'jenkinsSlaveCommon'),
                booleanParam(description: 'Jenkins slave for web', name: 'jenkinsWebNode'),
                booleanParam(description: 'Jenkins slave for Scala', name: 'jenkinsBeNode'),
                booleanParam(description: 'Custom RabbitMq with additional plugins', name: 'rabbitmq')
            ])
])

def emptyStage(String name) {
  stage(name) {
    echo "Skipping $name"
  }
}

def stageName(stageArgs) {
  return stageArgs[1] as String
}

def buildImage(stageArgs) {
  assert stageArgs[0]: "Argument for ${stageArgs[2]} must be true."
  stage(stageName(stageArgs)) {
    String tag = stageArgs[1]
    String dockerFileDir = stageArgs[2]
    Map buildArgs = null
    if (stageArgs[3] != null) {
      buildArgs = stageArgs[3]
    } else {
      buildArgs = [:]
    }
    echo "Building image ${tag} from ${dockerFileDir} with arguments: ${buildArgs}"
    docker.withRegistry(registryUrl, registryCredentialsId) {
      String baseImage = buildArgs['baseImage']
      def dockerImage = docker.build(tag, "${params.noCache ? '--no-cache' : ''} ${baseImage != null ? "--build-arg baseImage=${baseImage}" : ''} ${dockerFileDir}")
      dockerImage.push()
    }
  }
}

def pipeline() {

  imagesToBuild = [
      // build true/false | target image name with tag | Dockerfile source directory | build custom args (optional)
      [params.ubuntuXenial, 'ubuntu:xenial-custom', 'base/ubuntu', [baseImage: 'ubuntu:xenial']],
      [params.ubuntuBionic, 'ubuntu:bionic-custom', 'base/ubuntu', [baseImage: 'ubuntu:bionic']],
      [params.java13Bionic, 'java:13-bionic', 'base/java13', [baseImage: 'ubuntu:bionic-custom']],
      [params.sbt, 'sbt-base:java13', 'base/docker-sbt-base'],
      [params.jenkinsMaster, 'jenkins-docker:azure', 'jenkins/master'],
      [params.jenkinsSlaveCommon, 'jenkins-slave-common', 'base/jenkins-slave-common', [baseImage: 'java:13-bionic']],
      [params.jenkinsWebNode, 'jenkins-web-node:custom', 'jenkins/web-node'],
      [params.jenkinsBeNode, 'jenkins-be-node:custom', 'jenkins/be-node'],
      [params.rabbitmq, 'rabbitmq:3.7-custom', 'components/rabbit-custom'],
  ]
  imagesToBuild.each {
    if (it[0]) {
      buildImage(it)
    } else {
      emptyStage(stageName(it))
    }
  }
}

node('web-node') {
  try {
    checkout scm
    pipeline()
  } catch (Throwable caught) {
    error "Caught unexpected error: ${caught.message}"
    throw caught
  } finally {
    stage('Post actions') {
      echo 'Cleaning workspace'
      cleanWs()
    }
  }
}
